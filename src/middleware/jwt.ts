import express from 'express';
import debug from 'debug';
import jwt from 'jsonwebtoken';

const log: debug.IDebugger = debug('app:middleware');
const config = process.env;

interface JwtPayload {
    username: string;
    role: string;
    audience: string;
    expiresIn: string;
    exp: number;
}

function JwtMiddleware(req: express.Request, res: express.Response, next: express.NextFunction) {
    const token = req.body.token || req.query.token || req.headers["x-jwt-access-token"];
    if (!token) {
        return res.status(403).json({msg:"Token is required"})
    }
    try {
        const decoded = jwt.verify(token, config.TOKEN_KEY as string) as JwtPayload;
        if (decoded.role !== "admin") {
            return res.status(401).json({msg: "Unauthorized"})
        }
        req.app.set("user", decoded)
    } catch (err) {
        log(err)
        return res.status(401).json({msg:"Invalid Token"});
    }
    next();
}

export default JwtMiddleware;
