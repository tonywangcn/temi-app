import express from 'express';
import debug from 'debug';
import moment from 'moment';
import redis from 'redis';

const redisClient = redis.createClient({
    host: process.env.REDIS_HOST,
    port: parseInt( process.env.REDIS_PORT as string, 10),
    password: process.env.REDIS_PASS,
});

const log: debug.IDebugger = debug('app:middleware');

const WINDOW_DURATION_IN_MINUTES = 1;
const MAX_WINDOW_REQUEST_COUNT = 10;
const WINDOW_LOG_DURATION_IN_SECONDS = 10;

interface Stats {
    requestTimeStamp: number;
    requestCount: number;
}

// prefix for rate limiter key in redis
const redisPrefix = "redis_rate_limiter_"

// rate limit middleware
function RateLimitMiddleware(req: express.Request, res: express.Response, next: express.NextFunction) {
    const user = req.app.get("user")
    let data:Stats[] = []
    try {
        if (!redisClient) {
            log("Redis client is not initialized!")
            process.exit(1)
        }
        redisClient.get(redisPrefix + user.username, (err, record) => {
            if (err) {
                return res.status(400).json({msg: "Failed to get info from redis!"})
            }
            const currentTime = moment()
            if (record === null) {
                data = [{requestTimeStamp: currentTime.unix(), requestCount:1}];
                redisClient.set(redisPrefix+user.username, JSON.stringify(data));
                return next();
            }
            data = JSON.parse(record ?? "") as Stats[];

            const requestsInWindow = data.filter(entry => {
                return entry.requestTimeStamp > moment().subtract(WINDOW_DURATION_IN_MINUTES, "minutes").unix();
            }).sort((a, b) => a.requestTimeStamp - b.requestTimeStamp)

            log("requestsInWindow ", requestsInWindow, requestsInWindow.length)

            const totalWindowRequestsCount = requestsInWindow.reduce((accumulator, entry) => {
                return accumulator + entry.requestCount;
            },0)
            log("totalWindowRequestsCount ", totalWindowRequestsCount)
            if (totalWindowRequestsCount >= MAX_WINDOW_REQUEST_COUNT) {
                return res.status(429).json({msg: `You have exeeded the ${MAX_WINDOW_REQUEST_COUNT} requests in ${WINDOW_DURATION_IN_MINUTES} minutes limit!`});
            } else {
                const lastRequestLog = data.sort((a, b) => a.requestTimeStamp - b.requestTimeStamp)[data.length - 1];
                // const potentialCurrentWindowIntervalStartTimeStamp = currentTime.subtract( WINDOW_LOG_DURATION_IN_SECONDS, "seconds" ).unix();
                log("lastRequestLog ",lastRequestLog, "currentTime", currentTime.unix(), "subtract ", currentTime.subtract(WINDOW_LOG_DURATION_IN_SECONDS, "seconds").unix())
                if (lastRequestLog.requestTimeStamp > currentTime.subtract(WINDOW_LOG_DURATION_IN_SECONDS, "seconds").unix()) {
                    lastRequestLog.requestCount ++;
                    requestsInWindow[requestsInWindow.length - 1] = lastRequestLog;
                } else {
                    requestsInWindow.push({requestTimeStamp: currentTime.unix(), requestCount:1});
                }
                redisClient.set(redisPrefix+ user.username, JSON.stringify(requestsInWindow))
                log("username ",user.username,"record ", record, "requestsInWindow ",requestsInWindow, "totalWindowRequestsCount ",totalWindowRequestsCount)
                next();
            }
        })
    } catch (err) {
        return res.status(400).json({msg: "Failed to get info from redis!"})
    }
}

export default RateLimitMiddleware;
