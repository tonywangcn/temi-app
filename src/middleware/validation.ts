import express from 'express';
import debug from 'debug';


const log: debug.IDebugger = debug('app:middleware');

/**
 * Check if is array of number
 * @param {value}  any - input value
 * @return {boolean} - return value
 */
function isNumberArray(value: any) {
    if (Object.prototype.toString.call(value) === '[object Array]') {
        if (value.length < 1) {
            return false;
        } else {
            return value.every((d: any) => typeof d === "number");
        }
    }
    return false
}
// isNumberArray validation middleware
function IsNumberArrayValidationMiddleware(req: express.Request, res: express.Response, next: express.NextFunction) {
    const body = req.body as number[]
    log("body ", body)
    if (!isNumberArray(body)) {
        return res.status(403).json({msg: "Request Body is illegal!"});
    }
    next();

}

export default IsNumberArrayValidationMiddleware;
