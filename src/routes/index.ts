import * as express from "express";

import JwtMiddleware from "../middleware/jwt";
import { UserRegister, Process } from "../controllers";
import RateLimitMiddleware from "../middleware/ratelimit";
import IsNumberArrayValidationMiddleware from "../middleware/validation";

export const register = (app: express.Application) => {
    app.post("/register", UserRegister);
    app.post("/process",JwtMiddleware, RateLimitMiddleware, IsNumberArrayValidationMiddleware, Process);
};