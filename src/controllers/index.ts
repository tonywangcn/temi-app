import express from 'express';
import debug from 'debug';
import jwt from 'jsonwebtoken';

const log: debug.IDebugger = debug('app:controller');

export async function UserRegister(req: express.Request, res: express.Response) {
    const username = req.body.username
    const role  = req.body.role
    if (!role) {
        return res.status(400).json({msg: "role is required"})
    }
    if (!username) {
        return res.status(400).json({msg: "username is required"})
    }
    const token = jwt.sign({username: username as string,role: role as string, audience: "TEST"}, process.env.TOKEN_KEY as string, { expiresIn: "2h"});

    return res.status(200).send({token: token as string});
}

export async function Process(req: express.Request, res: express.Response) {
    const data = req.body as number[];
    const result = new Map<string, number>();
    for (const n of data) {
        if ((result.get(`${n}`)?? 0) === 0 ) {
            result.set(`${n}`,1);
        } else {
            result.set(`${n}`, (result.get(`${n}`) ?? 0) + 1);
        }
    }
    return res.json({result: Object.fromEntries(result) });
}