## Backend Interview Assignment

### Local Development

1. make dev
2. make exe
3. npm i
4. npm run start with nodemon
   
### Deploy to department

1. make prod


### Introduction

1. makefile command
2. docker-compose-prod.yml docker-compose file for production deployment
3. docker-compose.yml  docker-compose file for local development
4. .env environment for nodejs service
5. dist, js folder compiled from typescript
6. src -> project source code folder. 
   controllers -> folder for api logic
   middleware -> middleware for expressjs
   routes ->  expressjs router
7. tests -> folder for unite test


