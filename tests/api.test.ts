import request from 'supertest'
import app from '../index'
let token: string
beforeAll( async () => {
  const res = await request(app).post("/register").send({username: "test_user_1", role: "admin"})
  token = res.body.token
})
describe('API Endpoints', () => {
  it('page not found,should return 404', async () => {
    const res = await request(app)
      .get('/not_found')
      .send()
    expect(res.statusCode).toEqual(404)
    expect(res.body.msg).toContain("Page Not Found")
  })
  it("role is not admin, should return 401", async ()=> {
    let res = await request(app).post("/register").send({username: "test_user_1", role: "test"})
    res = await request(app)
      .post("/process")
      .set({"x-jwt-access-token": res.body.token})
      .set({"Content-Type":"application/json"})
      .send([1,3,5,3,1])
    expect(res.statusCode).toEqual(401)
  })
  it("validate request body,should return 403", async () => {
    const res = await request(app)
      .post("/process")
      .set({"x-jwt-access-token": token})
      .set({"Content-Type":"application/json"})
      .send([1,3,5,3,1,"1"])
    expect(res.statusCode).toEqual(403)
    expect(res.body.msg).toContain("illegal")
  })
  for (let n = 1; n < 10; n ++ ) {
    it("validate rate limit, should return 200 in first 10 requests, but failed afterwards.", async () => {
      const res = await request(app)
        .post("/process")
        .set({"x-jwt-access-token": token})
        .set({"Content-Type":"application/json"})
        .send([1,3,5,3,1])
      expect(res.statusCode).toEqual(200)
    })
  }
  it("validate rate limit, should return 429", async () => {
    const res = await request(app)
      .post("/process")
      .set({"x-jwt-access-token": token})
      .set({"Content-Type":"application/json"})
      .send([1,3,5,3,1])
    expect(res.statusCode).toEqual(429)
  })

})