dev:
	docker-compose up -d
down:
	docker-compose down
exe:
	docker exec -it temi-app bash

prod:
	docker-compose -f docker-compose-prod.yml up -d