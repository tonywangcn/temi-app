import express from "express";
import * as http from "http";
import * as bodyparser from 'body-parser';
import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import cors from 'cors';
import * as routes from "./src/routes";
import debug from 'debug';

const app: express.Application = express();
const server: http.Server = http.createServer(app);
const port = 3000;
const log: debug.IDebugger = debug('app');

app.use(bodyparser.json());
app.use(cors());

app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    )
}));

// Configure routes
routes.register(app);

// catch 404 error
app.use((req: any, res) => {
    return res.status(404).json({msg:"Page Not Found"});
    });

app.use(expressWinston.errorLogger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    )
}));

if (process.env.NODE_ENV !== "test") {
    server.listen(port, () => {
        log(`Server running at http://localhost:${port}`);
    });
}
export default app;